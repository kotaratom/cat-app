//
//  CatAPIProjectApp.swift
//  CatAPIProject
//
//  Created by Tomas Kotara on 12.06.22.
//

import SwiftUI

@main
struct CatAPIProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}


/*
 Sources:
 
 GitHub. 2022. GitHub - gahntpo/CatAPISwiftUI: demo: REST API with SwiftUI. [online] Available at: <https://github.com/gahntpo/CatAPISwiftUI> [Accessed 19 June 2022].
 */

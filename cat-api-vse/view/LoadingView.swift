//
//  LoadingView.swift
//  LoadingView
//
//  Created by Tomas Kotara on 12.06.22.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        VStack(spacing: 20)  {
            Text("🙀")
                .font(.system(size: 70))
            ProgressView()
            Text("Loading ...")
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}

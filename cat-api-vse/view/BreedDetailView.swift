//
//  BreedDetailView.swift
//  BreedDetailView
//
//  Created by Tomas Kotara on 12.06.22.
//

import SwiftUI

struct BreedDetailView: View {
    @State private var showWebView = false
    
    let breed: Breed
    let imageSize: CGFloat = 300
    
    var body: some View {
        ScrollView {
            VStack() {
                
                if breed.wikipediaURL != nil {
                    
                    // Link("Wiki", destination: URL(string: breed.wikipediaURL ?? "http")!)
                    
                    // WebView show
                    Button {
                        showWebView.toggle()
                    } label: {
                        Text("Wiki")
                    }
                    .sheet(isPresented: $showWebView) {
                        WebView(url: URL(string: breed.wikipediaURL ?? "http")!)
                    }
                    .buttonStyle(BorderedButtonStyle())
                }
                
            }
            
            VStack {
                if breed.image?.url != nil {
                    AsyncImage(url: URL(string: breed.image!.url!)) { phase in
                        if let image = phase.image {
                            image.resizable()
                                .scaledToFit()
                                .frame( height: imageSize)
                                .clipped()
                            
                        } else if phase.error != nil {
                            
                            Text(phase.error?.localizedDescription ?? "error")
                                .foregroundColor(Color.pink)
                                .frame(width: imageSize, height: imageSize)
                        } else {
                            ProgressView()
                                .frame(width: imageSize, height: imageSize)
                        }
                        
                    }
                }else {
                    Color.gray.frame(height: imageSize)
                }
                
                VStack(alignment: .leading, spacing: 15) {
                    
                    Text(breed.name)
                        .font(.headline)
                    
                    Text(breed.breedExplaination)
                    
                    HStack {
                        Text("Temperament:")
                        .font(.system(size: 16, weight: .bold))
                        
                        Text(breed.temperament)
                    }
                    
                    HStack {
                        Text("Life span:")
                        .font(.system(size: 16, weight: .bold))
                        
                        Text(breed.lifeSpan)
                    }
                    
                    HStack {
                        Text("Country of origin:")
                        .font(.system(size: 16, weight: .bold))
                        
                        Text(breed.origin)
                    }

                    if breed.isHairless {
                        Text("hairless")
                    }
                    if breed.isRare {
                        Text("rare")
                    }
                    
                    HStack {
                        Text("Energy level:")
                            .font(.system(size: 16, weight: .bold))
                        Spacer()
                        
                        ForEach(1..<6) { id in
                            Image(systemName: "star.fill")
                                .foregroundColor(breed.energyLevel > id ? Color.accentColor : Color.gray )
                        }
                    }
                    
                    Spacer()
                }
                .padding()
                    .navigationBarTitleDisplayMode(.inline)
            }
        }
    }
}

struct BreedDetailView_Previews: PreviewProvider {
    static var previews: some View {
        BreedDetailView(breed: Breed.example1())
    }
}

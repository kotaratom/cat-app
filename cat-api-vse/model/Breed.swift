//
//  CatBreed.swift
//  CatBreed
//
//  Created by Tomas Kotara on 12.06.22.
//

import Foundation


struct Breed: Codable, Identifiable {
    
    let id: String
    let name: String
    let temperament: String
    let breedExplaination: String
    let lifeSpan: String
    let origin: String
    var wikipediaURL: String?
    let energyLevel: Int
    let isRare: Bool
    let isHairless: Bool
    let image: BreedImage?
    
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case temperament
        case breedExplaination = "description"
        case energyLevel = "energy_level"
        case lifeSpan = "life_span"
        case origin
        case isRare = "rare"
        case wikipediaURL = "wikipedia_url"
        case isHairless = "hairless"
        case image
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        temperament = try values.decode(String.self, forKey: .temperament)
        lifeSpan = try values.decode(String.self, forKey: .lifeSpan)
        origin = try values.decode(String.self, forKey: .origin)
        breedExplaination = try values.decode(String.self, forKey: .breedExplaination)
        energyLevel = try values.decode(Int.self, forKey: .energyLevel)
        
        let hairless = try values.decode(Int.self, forKey: .isHairless)
        isHairless = hairless == 1
        
        let rare = try values.decode(Int.self, forKey: .isRare)
        isRare = rare == 1
        
        do {
            wikipediaURL = try values.decode(String.self, forKey: .wikipediaURL)
            print("good!")
        } catch {
            print(name)
            print("error here!")
        }
        
        image = try values.decodeIfPresent(BreedImage.self, forKey: .image)
    }
    
    init(name: String, id: String, explaination: String, temperament: String, lifeSpan: String, origin: String, wikipediaURL: String?,
         energyLevel: Int, isHairless: Bool, isRare: Bool, image: BreedImage?){
        self.name = name
        self.id = id
        self.breedExplaination = explaination
        self.energyLevel = energyLevel
        self.lifeSpan = lifeSpan
        self.origin = origin
        self.temperament = temperament
        self.wikipediaURL = wikipediaURL
        self.image = image
        self.isHairless = isHairless
        self.isRare = isRare
    }
    
 
    static func example1() -> Breed {
        return Breed(name: "Abyssinian",
                     id: "abys",
                     explaination: "The Abyssinian is easy to care for, and a joy to have in your home. They’re affectionate cats and love both people and other animals.",
                     temperament: "Active, Energetic, Independent, Intelligent",
                     lifeSpan: "3-5",
                     origin: "Slovenia",
                     wikipediaURL: "htttp",
                     energyLevel: 3,
                     isHairless: false,
                     isRare: false,
                     image: BreedImage(height: 100, id: "i", url: "https://cdn2.thecatapi.com/images/unX21IBVB.jpg", width: 100))
        
    }
    
    static func example2() -> Breed {
        return Breed(name: "Cyprus",
                     id: "cypr",
                     explaination: "Loving, loyal, social and inquisitive, the Cyprus cat forms strong ties with their families and love nothing more than to be involved in everything that goes on in their surroundings. They are not overly active by nature which makes them the perfect companion for people who would like to share their homes with a laid-back relaxed feline companion.",
                     temperament: "Affectionate, Social",
                     lifeSpan: "3-4",
                     origin: "Slovenia",
                     wikipediaURL: "htttp",
                     energyLevel: 4,
                     isHairless: false,
                     isRare: false,
                     image: BreedImage(height: 100, id: "i", url: "https://cdn2.thecatapi.com/images/unX21IBVB.jpg", width: 100))
        
    }
}



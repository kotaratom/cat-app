//
//  BreedImage.swift
//  BreedImage
//
//  Created by Tomas Kotara on 12.06.22.
//

import Foundation

struct BreedImage: Codable {
    let height: Int?
    let id: String?
    let url: String?
    let width: Int?
    
}

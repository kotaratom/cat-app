//
//  APIServiceProtocol.swift
//  APIServiceProtocol
//
//  Created by Tomas Kotara on 12.06.22.
//

import Foundation


protocol APIServiceProtocol {
    func fetchBreeds(url: URL?, completion: @escaping(Result<[Breed], APIError>) -> Void)
}

/*
 Sources:
 
 GitHub. 2022. GitHub - gahntpo/CatAPISwiftUI: demo: REST API with SwiftUI. [online] Available at: <https://github.com/gahntpo/CatAPISwiftUI> [Accessed 19 June 2022].
 */

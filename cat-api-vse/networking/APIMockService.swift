//
//  APIMockService.swift
//  APIMockService
//
//  Created by Tomas Kotara on 12.06.22.
//  Mocking data installation

import Foundation

struct APIMockService: APIServiceProtocol {
    
    var result: Result<[Breed], APIError>
    
    func fetchBreeds(url: URL?, completion: @escaping (Result<[Breed], APIError>) -> Void) {
        completion(result)
    }
    
}

/*
 Sources:
 
 GitHub. 2022. GitHub - gahntpo/CatAPISwiftUI: demo: REST API with SwiftUI. [online] Available at: <https://github.com/gahntpo/CatAPISwiftUI> [Accessed 19 June 2022].
 */

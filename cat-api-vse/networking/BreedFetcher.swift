//
//  BreedFetcher.swift
//  BreedFetcher
//
//  Created by Tomas Kotara on 12.06.22.
//

import Foundation


class BreedFetcher: ObservableObject {
    
    @Published var breeds = [Breed]()
    @Published var isLoading: Bool = false
    @Published var errorMessage: String? = nil
    
    let apiURL = "https://api.thecatapi.com/v1/breeds?api_key=43e31b72-b906-44c9-81c9-7300e95b2af7"
    let service: APIServiceProtocol
    
    init(service: APIServiceProtocol = APIService()) {
        self.service = service
        fetchAllBreeds()
    }
    
    func fetchAllBreeds() {
        
        isLoading = true
        errorMessage = nil
        
        let url = URL(string: apiURL)
        service.fetchBreeds(url: url) { [unowned self] result in
            
            DispatchQueue.main.async {
                
                self.isLoading = false
                switch result {
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    // print(error.description)
                    print(error)
                case .success(let breeds):
                    print("--- sucess with \(breeds.count)")
                    self.breeds = breeds
                }
            }
        }
        
    }
    
    
    //MARK: preview helpers
    
    static func errorState() -> BreedFetcher {
        let fetcher = BreedFetcher()
        fetcher.errorMessage = APIError.url(URLError.init(.notConnectedToInternet)).localizedDescription
        return fetcher
    }
    
    // importing fetch
    static func successState() -> BreedFetcher {
        let fetcher = BreedFetcher()
        fetcher.breeds = [Breed.example1(), Breed.example2()]
        
        return fetcher
    }
}


/*
 Sources:
 
 GitHub. 2022. GitHub - gahntpo/CatAPISwiftUI: demo: REST API with SwiftUI. [online] Available at: <https://github.com/gahntpo/CatAPISwiftUI> [Accessed 19 June 2022].
 */
